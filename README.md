# Nicholls et al 2022 Emulator Changes

|Info|Badge|
|---|---|
| Article | [![GRL](https://img.shields.io/badge/GRL-Nicholls%20et%20al.%20(2022)-informational)](https://doi.org/10.1029/2022GL099788) |
| Archive | [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6584386.svg)](https://doi.org/10.5281/zenodo.6584386) |
| License | TBD |

Code and data to create the publication [Nicholls et al., 2022, *GRL*](https://doi.org/10.1029/2022GL099788) on changes in emulators between SR1.5 and AR6.
If this repository is of use to you, please cite

The accepted manuscript is available at https://doi.org/10.1029/2022GL099788.
All code and data has also been archived at Zenodo (https://doi.org/10.5281/zenodo.6584385).

If you have any issues, please raise them in the [issue tracker](<https://gitlab.com/magicc/nicholls-et-al-2022-emulator-changes/issues>).

## Installation

### Git LFS

We use git lfs for large files. 
Installation instructions can be found [here](https://docs.github.com/en/repositories/working-with-files/managing-large-files/installing-git-large-file-storage). 
When you use the repository for the first time, make sure you run `git lfs install`.

### Required packages

The simplest way to get setup is using conda or mamba. Once you have conda or mamba installed, you
can simply run the following steps.

```sh
# create a new conda environment
mamba create --name nicholls-et-al-2022-emulator-changes
# activate it
mamba activate nicholls-et-al-2022-emulator-changes
# install dependencies
mamba env update --name nicholls-et-al-2022-emulator-changes -f environment.yml
# install pip dependencies and local package
pip install -e .
```

## Running the analysis

The analysis is all documented in notebooks. Simply run the notebooks in order.
To avoid having to re-run all the notebooks (which takes time), we have saved the data using git lfs. 
Running git lfs fetch --all will download all the data (warning, it is around one gigabyte).
