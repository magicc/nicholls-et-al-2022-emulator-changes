import os.path

import pandas as pd
import pyam
import scmdata
import scmdata.database


out_path = os.environ["OUT_DIR"]
print(out_path)
db = scmdata.database.ScmDatabase(out_path, levels=("variable",))

# delete any existing data
# db.delete()
if not db.available_data().empty:
    raise ValueError("database already contains data")


conn = pyam.iiasa.Connection()
print("Connecting to SR1.5 database")
conn.connect("iamc15")

print("Querying data")

variables_to_download = (
    "Emissions*",
    "*Concentration*",
    "*Temperature|Global Mean*",
    "*Temperature|Exceed*",
    "*Forcing*",
)
for v in variables_to_download:
    print(f"Downloading {v}")
    df = pyam.read_iiasa(
        "iamc15",
        variable=v,
        region="World",
        meta=["category", "Kyoto-GHG|2010 (SAR)"]
    )

    meta = df.meta.drop("exclude", axis="columns")
    df_with_meta = meta.join(df.timeseries().reset_index().set_index(meta.index.names))
    df_scmrun = scmdata.ScmRun(df_with_meta)

    print("Saving to database")
    db.save(df_scmrun)
