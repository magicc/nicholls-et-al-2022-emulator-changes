import pandas as pd
import pyam
import scmdata

from .compat import rename_model_to_sr15_names, rename_scenario_to_sr15_names


def default_warming_exceedance_prob(temperature):
    """
    Default mapping from a temperature level to the exceedance probability variable in the SR1.5 database

    Parameters
    ----------
    temperature : float, str
        Temperature level to check the exceedance probability of

    Returns
    -------
    str
        Variable in the SR1.5 database which identifies the exceedance
        probability
    """
    return "AR5 climate diagnostics|Temperature|Exceedance Probability|{} °C|MAGICC6".format(temperature)


def _check_for_tmp_in_meta(meta_df, col):
        if "tmp" in meta_df[col].tolist():
            raise AssertionError("All 'tmp' should have been handled in column: {}".format(col))


def sr15_categorisation(
    indf,
    warming_exceedance_prob=None,
):
    """
    Perform categorisation in the same way as was done in SR1.5

    Based on `<https://github.com/iiasa/ipcc_sr15_scenario_analysis/blob/master/assessment/sr15_2.0_categories_indicators.ipynb>`_

    Parameters
    ----------
    indf : :obj:`pyam.IamDataFrame`
        Timeseries to categorise

    warming_exceedance_prob : func, optional
        Function which takes a single argument, the temperature threshold, and
        returns the variable which specifies the timeseries which contains the
        probability of exceeding that warming threshold. For example, if we
        called ``warming_exceedance_prob(1.5)``, we would expect output like
        ``AR5 climate diagnostics|Temperature|Exceedance Probability|1.5 °C|MAGICC6``.
        If not supplied, the naming convention used in SR1.5 will be used
        (``utils.categorisation.default_warming_exceedance_prob``).

    Returns
    -------
    :obj:`pd.DataFrame`
        Categorisations (on ["model", "scenario"] basis)

    Notes
    -----
    Categorising based on timeseries of exceedance probabilities
    underestimates the true likelihood of exceedance. The reason is that the
    timeseries approach means you're looking for exceedance **at the same
    time** in each ensemble member. However, what you're really interested
    in is exceedance at any point in time, considered over all ensemble
    members.
    """
    if warming_exceedance_prob is None:
        _warming_exceedance_prob = default_warming_exceedance_prob
    else:
        _warming_exceedance_prob = warming_exceedance_prob

    out = indf.copy()
    uncat = "uncategorised"
    cat = "category"
    subcat = "subcategory"

    # sub-category names
    below_15i = "Below 1.5C (I)"
    below_15ii = "Below 1.5C (II)"
    lower15_low_overshoot = "Lower 1.5C low overshoot"
    higher15_low_overshoot = "Higher 1.5C low overshoot"
    lower15_high_overshoot = "Lower 1.5C high overshoot"
    higher15_high_overshoot = "Higher 1.5C high overshoot"
    lower2 = "Lower 2C"
    higher2 = "Higher 2C"
    above2 = "Above 2C"

    # category names
    below15 = "Below 1.5C"
    low_overshoot_15 = "1.5C low overshoot"
    high_overshoot_15 = "1.5C high overshoot"


    out.set_meta(name=cat, meta=uncat)
    out.set_meta(name=subcat, meta=uncat)

    # The order of operations is pretty delicate here. There is likely a way
    # to clarify what is going on and reduce the number of lines of code, but
    # that is a future refactoring.

    # 1.5 no overshoot
    pyam.categorize(
        out,
        value=below_15i,
        name=subcat,
        criteria={_warming_exceedance_prob(1.5): {"up": 0.34}},
        exclude=False,
        subcategory=uncat,
    )
    # next any scenarios with 1.5 exceedance probability between 0.34 and 0.5
    # (the previous categorisation, combined with the subcategory filter
    # ensures that no scenarios categorised as 1.5 exceedance probability less
    # than 0.34 are caught here)
    pyam.categorize(
        out,
        value=below_15ii,
        name=subcat,
        criteria={_warming_exceedance_prob(1.5): {"up": 0.5}},
        exclude=False,
        subcategory=uncat,
    )

    # 1.5 low overshoot
    pyam.categorize(
        out,
        value="tmp",
        name=subcat,
        criteria={_warming_exceedance_prob(1.5): {"up": 0.67}},
        exclude=False,
        subcategory=uncat,
    )
    pyam.categorize(
        out,
        value=lower15_low_overshoot,
        name=subcat,
        criteria={_warming_exceedance_prob(1.5): {"up": 0.34, "year": 2100}},
        exclude=False,
        subcategory="tmp",
    )
    pyam.categorize(
        out,
        value=higher15_low_overshoot,
        name=subcat,
        criteria={_warming_exceedance_prob(1.5): {"up": 0.5, "year": 2100}},
        exclude=False,
        subcategory="tmp",
    )
    out.set_meta(meta=uncat, name=subcat, index=out.filter(subcategory="tmp"))
    _check_for_tmp_in_meta(out.meta, subcat)

    # 1.5 high overshoot
    ts = out.filter(
        exclude=False,
        subcategory=uncat,
        variable=_warming_exceedance_prob(1.5),
    ).timeseries()

    out.set_meta(
        meta="tmp",
        name=subcat,
        index=ts[ts.apply(lambda x: max(x), axis=1) > 0.66].index,
    )
    pyam.categorize(
        out,
        value=lower15_high_overshoot,
        name=subcat,
        criteria={_warming_exceedance_prob(1.5): {"up": 0.34, "year": 2100}},
        exclude=False,
        subcategory="tmp",
    )
    pyam.categorize(
        out,
        value=higher15_high_overshoot,
        name=subcat,
        criteria={_warming_exceedance_prob(1.5): {"up": 0.5, "year": 2100}},
        exclude=False,
        subcategory="tmp",
    )
    out.set_meta(meta=uncat, name=subcat, index=out.filter(subcategory="tmp"))
    _check_for_tmp_in_meta(out.meta, subcat)

    # 2 categories
    pyam.categorize(
        out,
        value=lower2,
        name=subcat,
        criteria={_warming_exceedance_prob(2.0): {"up": 0.34}},
        exclude=False,
        subcategory=uncat,
    )
    pyam.categorize(
        out,
        value=higher2,
        name=subcat,
        criteria={_warming_exceedance_prob(2.0): {"up": 0.5}},
        exclude=False,
        subcategory=uncat,
    )
    pyam.categorize(
        out,
        value=above2,
        name=subcat,
        criteria={_warming_exceedance_prob(2.0): {"up": 1.0}},
        exclude=False,
        subcategory=uncat,
    )

    # aggregate to category
    out.set_meta(
        meta=below15,
        name=cat,
        index=out.filter(subcategory=[below_15i, below_15ii]).meta.index,
    )
    out.set_meta(
        meta=low_overshoot_15,
        name=cat,
        index=out.filter(subcategory=[lower15_low_overshoot, higher15_low_overshoot]).meta.index,
    )
    out.set_meta(
        meta=high_overshoot_15,
        name=cat,
        index=out.filter(subcategory=[lower15_high_overshoot, higher15_high_overshoot]).meta.index,
    )
    out.set_meta(
        meta=out.filter(subcategory=[lower2, higher2, above2])[subcat],
        name=cat,
    )

    out_df = out.meta.drop("exclude", axis="columns").reset_index()

    return out_df


def _sort_indexes(idf, order):
    out = idf.copy()

    if isinstance(idf.index, pd.MultiIndex):
        for i in range(len(idf.index.names)):
            out = out.reindex(order, level=i)
    else:
        out = out.reindex(order)

    return out


def determine_categorisation_move_overview(
    a,
    a_label,
    b,
    b_label,
    idx=["model", "scenario"],
    extra_group_out=[],
):
    meth1 = "Method 1"
    meth2 = "Method 2"
    combined_categorisations = pd.DataFrame(
        {
            meth1: a.set_index(idx)["category"],
            meth2: b.set_index(idx)["category"],
        }
    )
    combined_categorisations["{} - {}".format(a_label, b_label)] = 1

    return combined_categorisations.groupby([meth1, meth2] + extra_group_out).count()


def load_categorisation(fp, harmonise_names=True):
    categorisation = pd.read_csv(fp)
    
    ar6_categories = categorisation.loc[categorisation["methodology"] == "AR6", :].copy()
    if harmonise_names:
        ar6_categories.loc[:, "scenario"] = ar6_categories["scenario"].apply(
            rename_scenario_to_sr15_names
        )
        ar6_categories.loc[:, "model"] = ar6_categories["model"].apply(
            rename_model_to_sr15_names
        )

    sr15_categories = categorisation.loc[categorisation["methodology"] == "SR1.5", :].copy()
    
    return pd.concat([ar6_categories, sr15_categories])


def add_categorisation_to_scmrun(in_run, categorisation_table, idx=["model", "scenario"]):   
    return scmdata.ScmRun(
        in_run.timeseries().reset_index().set_index(idx).join(categorisation_table)
    )
