from ._version import get_versions
__version__ = get_versions()['version']
del get_versions

import os.path
import scmdata


from .compat import (
    rename_model_to_sr15_names,
    rename_scenario_to_sr15_names
)

DATA_DIR = os.path.join(os.path.dirname(__file__), "..", "..", "data")
FIGURES_DIR = os.path.join(os.path.dirname(__file__), "..", "..", "figures")

AR6_OUTPUT_SR15_RAW_ID = "20211014-sr15-no-harmonisation-nonco2"
AR6_OUTPUT_SR15_RAW_RCMIP_PHASE_2_CONFIG_ID = "20211014-sr15-no-harmonisation-nonco2-rcmip-phase-2"

OBS_DB_ID = "obs-v1"

MAGICC_VERSION = "MAGICCv7.5.3"

MOD_SCEN_DELIMITER = "-"


def join_mod_scen(mod, scen):
    return mod + MOD_SCEN_DELIMITER + scen


def load_with_mod_scen(db, harmonise_model_scenario_names=False, **kwargs):
    out = db.load(**kwargs)
    
    if harmonise_model_scenario_names:
        out["scenario"] = out["scenario"].apply(rename_scenario_to_sr15_names)
        out["model"] = out["model"].apply(rename_model_to_sr15_names)
        
    out["model_scenario"] = out["model"] + MOD_SCEN_DELIMITER + out["scenario"]
    
    return out


def copy_sr15_table_24_metadata_throughout(
    inp, 
    cols_to_copy=("Kyoto-GHG|2010 (SAR)", "SR1.5 Table 2.4", "aim_over_sampling")
):
    tmp = inp.timeseries().reset_index().set_index(["model", "scenario"])
    for c in cols_to_copy:
        c_vals = (
            tmp[c].reset_index().dropna().drop_duplicates().set_index(["model", "scenario"])
        )
        tmp = tmp.drop(c, axis="columns")
        tmp = tmp.join(c_vals)

    out = scmdata.ScmRun(tmp)

    return out
