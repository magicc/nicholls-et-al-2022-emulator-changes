import numpy.testing as npt
import pandas as pd
import scmdata
import tqdm.autonotebook as tqdman


def adjust_median_to_target(inp, eval_period, target, groups, check_groups_identical=True):
    out = []
    
    for i, (_, checker) in tqdman.tqdm(enumerate(inp.timeseries(time_axis="year").groupby(groups)), leave=False):
        current_median = (
            checker.loc[:, eval_period].mean(axis=1).median()
        )

        if i > 0 and check_groups_identical:
            npt.assert_allclose(check_median, current_median, atol=0.01)
        else:
            check_median = current_median

        shift = current_median - target
        out.append(checker - shift)

    return scmdata.ScmRun(pd.concat(out))
