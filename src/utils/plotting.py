CATEGORY_ORDER = [
    "Below 1.5C",
    "1.5C low overshoot",
    "1.5C high overshoot",
    "Lower 2C",
    "Higher 2C",
    "Above 2C",
]

CATEGORY_COLOURS = {
    "Below 1.5C": "xkcd:baby blue",
    "1.5C low overshoot": "xkcd:bluish",
    "1.5C high overshoot": "xkcd:darkish blue",
    "Lower 2C": "xkcd:orange",
    "Higher 2C": "xkcd:red",
    "Above 2C": "darkgrey",
    
}

ID_COLOURS = {
    "AR6 calibration SR1.5 processing MAGICCv7.5.3": "tab:blue",
    "AR6 calibration SR1.5 processing FaIRv1.6.2": "tab:red",
    "SR1.5 calibration SR1.5 processing FaIR1.3": "tab:gray",
    "RCMIP Phase 2 calibration SR1.5 processing MAGICCv7.5.3": "tab:green",
    "SR1.5 calibration SR1.5 processing MAGICC6": "tab:pink",
}

ID_MAP = {
    "AR6 calibration SR1.5 processing FaIRv1.6.2": "FaIRv1.6.2",
    "AR6 calibration SR1.5 processing MAGICCv7.5.3": "MAGICCv7.5.3",
    "RCMIP Phase 2 calibration SR1.5 processing MAGICCv7.5.3": "AR5-like MAGICCv7.5.3",
    "SR1.5 calibration SR1.5 processing FaIR1.3": "FaIR1.3",
    "SR1.5 calibration SR1.5 processing MAGICC6": "MAGICC6",
}


def short_id(idin):
    return ID_MAP[idin]
